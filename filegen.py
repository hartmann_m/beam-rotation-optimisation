# kernel e.g. datascience_py37
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import fileinput



# ?np.zeros

rot_radius = 13
rot_std = 6

nrPoints = 100
angleStep = 1
particles = np.zeros((nrPoints * 360//angleStep,6))

# +
wobble_angle = (rot_radius/2)*10**-3 
pi = np.arccos(-1)

for i,angle in enumerate(np.arange(0,360,angleStep)):
    x0=np.cos(pi*angle/180)*rot_radius
    y0=np.sin(pi*angle/180)*rot_radius
    x = np.random.default_rng().normal(x0, rot_std, nrPoints)
    y = np.random.default_rng().normal(y0, rot_std, nrPoints)
    xp = x0 / rot_radius * wobble_angle
    yp = y0 / rot_radius * wobble_angle
    xp = np.sin(xp)
    yp = np.sin(yp)
    zp = np.cos(wobble_angle) 
    particles[i*nrPoints:(i+1)*nrPoints,:] = np.array([x,np.zeros(nrPoints),y,np.ones(nrPoints)*xp,np.ones(nrPoints)*zp, np.ones(nrPoints)*yp]).T
# -
## beam travelling in z but y in MCNP 


#plt.hist(zp);

plt.xlabel('${radius\ [mm]}$',size=11)
plt.ylabel('$norm.\ beam\ intensity$',size=11)
plt.title('Sliced beam distribution')

np.savetxt("flattened.csv", particles, delimiter=" ")
df = pd.read_csv('flattened.csv', sep=" ", header=None)
df.columns = ["x", "y", "z", "xp","yp","zp"]
df['x'] = df['x'].div(10)
df['y'] = df['y'].div(10)
df['z'] = df['z'].div(10)
df2 = df[["xp", "yp","zp"]]
df = df.drop('xp', 1)
df = df.drop('yp', 1)
df = df.drop('zp', 1)
df=df.append(df2)
df = df.round(11)
df.insert(0, 'BlS1', '')
df.insert(1, 'BlS2', '')
df.insert(2, 'BlS3', '')
df.insert(3, 'BlS4', '')
df.insert(4, 'BlS5', '')
df.to_csv('flat.txt', sep=' ', float_format='%.11e',index=False, header=None)

lines = ['SDEF PAR=h POS=D1 VEC=FPOS D2 DIR=-1 ERG=590 wgt=1', 'SP1 1 35999r','SI1 L ']

with open("flat.txt", 'r+') as file:
    readcontent = file.read() 
    all_lines=file.readlines()                             
    file.seek(0, 0) 
    for i in lines:        
        file.write(str(i) + "\n")
    file.write(readcontent) 

exp = 36003
with open ('flat.txt','r') as b:
    lines = b.readlines()
with open('flat.txt','w') as b:
    for i,line in enumerate(lines):
        if i == exp:
            b.write('DS2 L'+"\n")
        b.write(line)

