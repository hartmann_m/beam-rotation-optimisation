# kernel e.g. datascience_py37
import numpy as np
import matplotlib.pyplot as plt

def norm(data):
    return(data)/(sum(data))


def makeGaussian2(x_center=0, y_center=0, theta=0, sigma_x = 10, sigma_y=10, x_size=640, y_size=640):
    # x_center and y_center will be the center of the gaussian, theta will be the rotation angle
    # sigma_x and sigma_y will be the stdevs in the x and y axis before rotation
    # x_size and y_size give the size of the frame 

    theta = 2*np.pi*theta/360
    x = np.arange(-x_size+0.5,x_size+0.5, 1, float)
    y = np.arange(-y_size+0.5,y_size+0.5, 1, float)
    y = y[:,np.newaxis]
    sx = sigma_x
    sy = sigma_y
    x0 = x_center
    y0 = y_center

    # rotation
    a=np.cos(theta)*x -np.sin(theta)*y
    b=np.sin(theta)*x +np.cos(theta)*y
    a0=np.cos(theta)*x0 -np.sin(theta)*y0
    b0=np.sin(theta)*x0 +np.cos(theta)*y0

    return np.exp(-(((a-a0)**2)/(2*(sx**2)) + ((b-b0)**2) /(2*(sy**2))))


framesize=30

static_std = 8
static_radius = 0

rot_radius = 13
rot_std = 6

static   = np.zeros((2*framesize,2*framesize))
rotation = np.zeros((2*framesize,2*framesize))

# +
pi = np.arccos(-1)

for angle in np.arange(0,720,1):
    x0=np.cos(pi*angle/180)*rot_radius
    y0=np.sin(pi*angle/180)*rot_radius
    rotation += makeGaussian2(x0, y0, angle, rot_std, rot_std, framesize, framesize)
# -


for angle in np.arange(0,360,1):
    x0=np.cos(pi*angle/180)*static_radius
    y0=np.sin(pi*angle/180)*static_radius
    static += makeGaussian2(x0, y0, angle, static_std, static_std, framesize, framesize)


bins = np.arange(-framesize+0.5,framesize+0.5,1)
values_rot  = norm(rotation[framesize,:])
values_stat = norm(static  [framesize,:])
plt.plot(bins, values_rot,  color='green', linestyle='--', linewidth=3, label='Rotating beam', marker="x", markerfacecolor='red')
plt.plot(bins, values_stat, color='red',   linestyle='--', linewidth=3, label='Static beam',   marker="x", markerfacecolor='black')
plt.ylim(bottom=0)
plt.legend()
plt.xlabel('${radius\ [mm]}$',size=11)
plt.ylabel('$norm.\ beam\ intensity$',size=11)
plt.title('Sliced beam distribution')
plt.axvline(x= 23,color='black',linestyle='--')
plt.axvline(x=-23,color='black',linestyle='--')
plt.text( 23,0.025,'target',ha='center',va='center',rotation='vertical',backgroundcolor='white',bbox={'facecolor':'white','pad':5})
plt.text(-23,0.025,'target',ha='center',va='center',rotation='vertical',backgroundcolor='white',bbox={'facecolor':'white','pad':5})
plt.savefig("15.11beamprofile.png",dpi=400)

values_stat

mean_rot  = np.average(bins, weights=values_rot)
mean_stat = np.average(bins, weights=values_stat)
print(mean_rot, mean_stat)

var_rot  = np.average((bins - mean_rot) **2, weights=values_rot)
var_stat = np.average((bins - mean_stat)**2, weights=values_stat)
print(var_rot, var_stat)

std_rot  = np.sqrt(var_rot)
std_stat = np.sqrt(var_stat)
print(std_rot, std_stat)



fig= plt.figure(figsize=(10,10))
rows=2
columns=2

#relative intensities per sq.mm
int_rot    = rotation[30,30]/np.sum(rotation)
int_static =   static[30,30]/np.sum(static)
print(int_rot)
print(int_static)
print("Reduction of peak is", int_rot/int_static*100,"%" )

#Plot 2D Gaussians for unrotated and rotated beams
fig.add_subplot(rows,columns,1)
plt.imshow(static)
plt.title('2D Gaussian beam distribution (x,y) - static beam')
plt.clim(0,100)
plt.colorbar();

fig.add_subplot(rows,columns,4)
plt.imshow(rotation)
plt.title('2D Gaussian beam distribution (x,y) - rotating beam')
plt.clim(0,100)
plt.colorbar();

# # Particle creation

# ## With Gaussian

mu, sigma = 9, 6 # mean and standard deviation
s = np.random.default_rng().normal(mu, sigma, 10000)

abs(mu - np.mean(s))

s

plt.hist(s)

# ## One by one

# Distribution constants
length_wobble_target = 8.35 # [m] length from wobbler to target, to be checked
rot_radius = 9 # [mm]
wobble_angle = rot_radius / length_wobble_target * 1e-3 # before 4.5e-3 # [rad]
static_std = 6 # [mm] symmetric in both planes
length_triplet_target = length_wobble_target + 1.1 # [m] length from triplet to target
beamsize_x_triplet = 0.317 # [mm] TO BE CONFIRMED 
beamsize_y_triplet = 2*beamsize_x_triplet
x_angle = static_std / length_triplet_target / beamsize_x_triplet * 1e-3  # 2e-3 # [rad]
y_angle = static_std / length_triplet_target / beamsize_y_triplet * 1e-3
print("wobbled_angle", wobble_angle)
print("x_angle", x_angle)
print("y_angle", y_angle)


def createParticles(nrPoints, angleStep):
    particles = np.zeros((nrPoints * 360//angleStep,6))
    for i,angle in enumerate(np.arange(0,360,angleStep)):
        # first create 2d distribution
        mean = [0, 0]
        # TODO: check covariance matrix at the target!!
        # for now a diagonal covariance matrix is assumed
        cov_x = [[static_std**2, 0], [0, x_angle**2]] 
        cov_y = [[static_std**2, 0], [0, y_angle**2]] 
        x, xp = np.random.multivariate_normal(mean, cov_x, nrPoints).T
        y, yp = np.random.multivariate_normal(mean, cov_y, nrPoints).T
        # add wobbled angle
        xp += np.cos(np.pi*angle/180) * wobble_angle
        yp += np.sin(np.pi*angle/180) * wobble_angle
        # add position from wobbling
        x  += np.cos(np.pi*angle/180) * rot_radius
        y  += np.sin(np.pi*angle/180) * rot_radius 
        # directional cosines
        xp = np.sin(xp)
        yp = np.sin(yp)
        zp = np.sqrt(1- xp*xp - yp*yp) #np.cos(wobble_angle)
        particles[i*nrPoints:(i+1)*nrPoints,:] = np.array([x,np.zeros(nrPoints),y,np.ones(nrPoints)*xp,np.ones(nrPoints)*zp, np.ones(nrPoints)*yp]).T
    return particles


particles = createParticles(nrPoints = 100,angleStep = 1)

particles[0]

np.sqrt(particles[:,3]* particles[:,3] + particles[:,4] * particles[:,4] + particles[:,5] * particles[:,5])

plt.xlabel("x")
plt.ylabel("y")
plt.hist2d(particles[:,0], particles[:,2], bins = 50);

plt.xlabel("x")
plt.ylabel("xp")
plt.hist2d(particles[:,0], particles[:,3], bins = 50);

plt.xlabel("y")
plt.ylabel("yp")
plt.hist2d(particles[:,2], particles[:,5], bins = 50);

plt.title("rotation")
plt.xlabel("sin(xp)")
plt.ylabel("sin(yp)")
plt.hist2d(particles[:,3], particles[:,5], bins = 50);

plt.title("sin(xp)")
plt.hist(particles[:,3]);

bins = np.arange(-framesize+0.5,framesize+0.5,1)
plt.xlabel("x [mm]")
plt.hist(particles[:,0], bins = bins, density = True);

plt.xlabel("y [mm]")
plt.hist(particles[:,2], bins = bins, density = True);

# Plot along radius, both left and right (hence 180 degrees)
particles_radius = createParticles(10000,180)
plt.xlabel("radius [mm]")
plt.hist(particles_radius[:,0], bins = bins, density = True);
#plt.plot(bins, values_stat, color='red',   linestyle='--', linewidth=3, label='Static beam',   marker="x", markerfacecolor='black')
plt.legend()
plt.axvline(x= 23,color='black',linestyle='--')
plt.axvline(x=-23,color='black',linestyle='--')
plt.text( 23,0.025,'target',ha='center',va='center',rotation='vertical',backgroundcolor='white',bbox={'facecolor':'white','pad':5})
plt.text(-23,0.025,'target',ha='center',va='center',rotation='vertical',backgroundcolor='white',bbox={'facecolor':'white','pad':5})

particles

# # MCNP format

# +
# Simple in mm
#np.savetxt("flattened.csv", particles, delimiter=",")
# -

import pandas as pd
def MCNPsave():
    '''
    Save in MCP format
    beam travelling in z but y in MCNP
    unit cm
    first positions then directional cosines
    '''
    np.savetxt("flattened.csv", particles, delimiter=" ")
    df = pd.read_csv('flattened.csv', sep=" ", header=None)
    df.columns = ["x", "y", "z", "xp", "yp", "zp"]
    # convert to cm
    df['x'] = df['x'].div(10)
    df['y'] = df['y'].div(10)
    df['z'] = df['z'].div(10)
    df2 = df[["xp", "yp","zp"]]
    df = df.drop('xp', 1)
    df = df.drop('yp', 1)
    df = df.drop('zp', 1)
    df=df.append(df2)
    df = df.round(11)
    df.insert(0, 'BlS1', '')
    df.insert(1, 'BlS2', '')
    df.insert(2, 'BlS3', '')
    df.insert(3, 'BlS4', '')
    df.insert(4, 'BlS5', '')
    df.to_csv('flat.txt', sep=' ', float_format='%.11e',index=False, header=None)

    # Add header
    lines = ['SDEF PAR=h POS=D1 VEC=FPOS D2 DIR=-1 ERG=590 wgt=1', 'SP1 1 35999r','SI1 L ']
    with open("flat.txt", 'r+') as file:
        readcontent = file.read()
        all_lines=file.readlines()
        file.seek(0, 0)
        for i in lines:
            file.write(str(i) + "\n")
        file.write(readcontent)

    exp = 36003
    with open('flat.txt','r') as b:
        lines = b.readlines()
    with open('flat.txt','w') as b:
        for i,line in enumerate(lines):
            # Add header for directions
            if i == exp:
                b.write('DS2 L'+"\n")
            b.write(line)


MCNPsave()
